#Importamos el controlador
import controlador

#Importamos BD
import pandas as pd

#Función para leer todas las tareas
def mostrar_clientes(clientes: dict):
    #Itera el diccionario principal
    for clave, valor in clientes.items():
        print(clave, ": ")
        #Itera el diccionario secundario
        for clave_sec, valor_sec in valor.items():
            print(clave_sec, " ->", valor_sec)
        print("-----------------------------")

        '''
        print("----------------------")
        print("Nombre del cliente: ", clientes["nombre"])
        print("Apellido del cliente: ", clientes["apellido"])
        print("Tipo de documento: ", clientes["td"])
        print("Número de documento: ", clientes["nd"])
        print("Email: ", clientes["email"])
        print("Celular: ", clientes["celular"])
        print("----------------------")
        '''

#Menú de opciones
def menu():
    #Inicializo el diccionario
    clientes = controlador.read()
    cont: int = 3
    opc = -1
    
    #clave = list(map(int, clientes.keys()))
    #increment = max(clave) + 1
    
    while opc != 0:
        print("***********************************")
        print("1 -> Crear cliente")
        print("2 -> Consultar todos los clientes")
        print("3 -> Actualizar cliente")
        print("4 -> Eliminar cliente")
        print("5 -> Guardar todos los cambios")
        print("0 -> Salir")
        print("***********************************")

        #Solicita la opción al usuario
        opc = int( input("Por favor ingrese una opción: ") )
        #Opciones
        if opc == 1:
            nombre: str = input("Por favor ingrese el nombre del cliente: "),
            apellido: str = input("Por favor ingrese el apellido del cliente: "),
            td: str = input("Ingrese su Tipo de documento: "),
            nd: str = input("Ingrese su Numero de documento: "),
            email: str = input("Ingrese su email: "),
            celular: int = ( input("ingrese su celular: ") )
            #Crea diccionario con los datos del empleado
            nuevo_cliente = {
                "nombre": nombre,
                "apellido": apellido,
                "td": td,
                "nd": nd,
                "email": email,
                "celular": celular
            }
            clientes= controlador.create(clientes, "0"+ str(cont), nuevo_cliente)
            cont += 1
            #increment += 1

        elif opc == 2:
            mostrar_clientes(clientes)

        elif opc == 3:
            clave: str = input("Por favor ingrese el id del empleado a actualizar: ")
            llaves = clientes.keys()
            if clave in llaves: 
                nombre: input("Por favor ingrese el nombre: ")
                apellido: input("Por favor ingrese el apellido: ")
                td: input("Por favor ingrese el tipo de documento: ")
                nd: int(input("Por favor ingrese el número de documento: "))
                email: input("Por favor ingrese el correo electrónico: ")
                celular: int( input("Número de celular: ") )
                #Crea un nuevo diccionario con los datos del empleado
                cliente_actualizado = {
                "nombre": nombre,
                "apellido": apellido,
                "td": td,
                "nd": nd,
                "email": email,
                "celular": celular
                }
                clientes = controlador.update(clientes, clave, cliente_actualizado)
            else:
                print("La clave ingresada no existe")
        
        elif opc == 4:
            clave = input("Por favor ingrese el id del nuevo empleado a eliminar: ")
            llaves= clientes.keys()
            if clave in llaves:
                clientes = controlador.delete(clientes, clave)
            else:
                print("La clave ingresada no existe")
                continue
        
        elif opc == 5:
            band = controlador.save(clientes)
            if band:
                print("-----------------")
                print("Datos guardados exitosamente")
                print("-----------------")
            else:
                print("-----------------")
                print("Vuelve a intentar")
                print("-----------------")

menu()