#Importamos biblioteca manejadora de formato json
import json
import pandas as pd

#Función para crear un nuevo cliente
def create(clientes:dict, clave: str, valor: dict)->dict:
    #Valida si la clave existe en el diccionario
    if not clave in clientes.keys():
        #Añadir una nueva clave-valor al diccionario
        clientes[clave] = valor
    return clientes

#Función para obtener(leer) todos los clientes
def read()->dict:
    datos = None
    #try-except para el manejo de excepciones (errores en ejecución)
    try:
        #Abrir archivo en el formato deseado
        with open("modelo.json") as modelo:
        #with open ("tarea_juan.csv", names=['nombre','apellido','email', 'nid', 'tid', 'celular']) as modelo:
            #Carga los datos en formato json y los asigna a una variable
            datos = json.load(modelo)
    except:
        print("Error")
        datos = {}

    return datos

#Función para actualizar la informacion de un clente
def update(clientes: dict, clave: str, valor: dict)->dict:
    llaves = clientes.keys()
    #Valida si la clave existe en el diccionario
    if clave in llaves:
        clientes[clave] = valor
    return clientes

#Función para eliminar un cliente
def delete(clientes: dict, clave: str)->dict:
    llaves = clientes.keys()
    #Valida si la clave existe en el diccionario
    if clave in llaves:
        #Elimino un cliente por medio de su clave
        del clientes [clave]
    return clientes

def save(datos: dict):
    band: bool = False
    try:
        #Referencio un fichero, si existe entonces lo actualiza, sino crea un nuevo archivo json
        with open("modelo.json", "w") as archivo:
        #escribe los datos en el archivo
            json.dump(datos, archivo)
            band = True

    except:
        band= False
        print("Error al momento de escribir fichero")

    return band